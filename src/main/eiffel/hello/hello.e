note
	description: "Esempio base per iniziare"
	author: "Mattia Monga"

class
	HELLO

create
	make

feature -- sezione: puo` contenere molte feature

	make
		local
			s: STRING
		do
			s := "Hello"
			answer := 42
			print (s + " World! (con print)%N")
			io.put_string ("The answer is: " + answer.out);
			io.put_new_line
			check
				the_answer_is_right: answer = 42
			end
		end

    answer: INTEGER
end
